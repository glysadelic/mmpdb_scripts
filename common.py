from rdkit import Chem
from rdkit.Chem.rdmolops import SanitizeFlags


def fix_smiles(smi_in: str):
    mol = Chem.MolFromSmiles(smi_in)
    if mol:
        return smi_in
    mol = Chem.MolFromSmiles(smi_in, sanitize=False)
    if mol:
        flags = SanitizeFlags.SANITIZE_ALL ^ SanitizeFlags.SANITIZE_PROPERTIES
        mol.UpdatePropertyCache(False)
        Chem.SanitizeMol(mol, flags)
        edited = False
        for atom in mol.GetAtoms():
            if atom.GetAtomicNum() == 7 and atom.GetExplicitValence() == 4 and atom.GetFormalCharge() == 0:
                atom.SetFormalCharge(1)
                edited = True
        if edited:
            smi_out = Chem.MolToSmiles(mol)
            mol = Chem.MolFromSmiles(smi_out)
            if mol:
                print('edited smiles {} to {}'.format(smi_in, smi_out))
                return smi_out
    print('failed to correct smiles {}'.format(smi_in))
    return smi_in

