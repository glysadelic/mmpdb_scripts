import argparse
import os.path
import common
from typing import List, Optional

from rdkit import Chem


def mol_property(mol, field_name):
    if not mol.HasProp(field_name):
        return '*'
    else:
        return mol.GetProp(field_name)


def process_sdf(file: str, id_field: str, field_names: List[str],
                smiles_file: Optional[str], activity_file: Optional[str]) -> None:
    assert file.endswith('.sdf')

    if not smiles_file:
        smiles_file = os.path.basename('{}.smi'.format(file[:-4]))
    if not activity_file:
        activity_file = os.path.basename('{}.props'.format(file[:-4]))

    with Chem.SDMolSupplier(file) as supplier, open(smiles_file, 'w') as sf, open(activity_file, 'w') as af:

        af.write('ID')
        for f in field_names:
            af.write(' {}'.format(f.replace(' ', '_')))
        af.write('\n')

        ids = set()
        smi_set = set()

        for mol_number, mol in enumerate(supplier):

            if not mol:
                print('Failed to parse entry for molecule number {}'.format(mol_number))
                continue

            if (mol.HasProp(id_field)):
                id = mol.GetProp(id_field)
            else:
                raise ValueError('Missing id for molecule number {}'.format(mol_number))
            if id in ids:
                raise ValueError('Duplicate entries for id {}'.format(id))
            ids.add(id)
            smiles = Chem.MolToSmiles(mol)
            if smiles in smi_set:
                print('Duplicate entries for smiles {}'.format(smiles))

            smi_set.add(smiles)
            smiles = common.fix_smiles(smiles)

            properties = [mol_property(mol, f) for f in field_names]
            af.write('{} {}\n'.format(id, ' '.join(properties)))
            sf.write('{} {}\n'.format(smiles, id))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Create input MMPDB files from CSV file')
    parser.add_argument('--file', type=str, help='CSV input file', required=True)
    parser.add_argument('--id_field', type=str,
                        help='SD field for structure id', required=True)
    parser.add_argument('--field_names', nargs='*', type=str, default=None, required=True,
                        help='List of fields in the sdf file that will be as used properties')
    parser.add_argument('--smiles_file', type=str, default=None, required=False,
                        help='Optional smiles output file- defaults to basename from sdf file')
    parser.add_argument('--activity_file', type=str, default=None, required=False,
                        help='Optional properties output file- defaults to basename from sdf file')
    args = parser.parse_args()

    process_sdf(args.file, args.id_field, args.field_names,
                args.smiles_file, args.activity_file)
