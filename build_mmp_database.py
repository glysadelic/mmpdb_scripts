import argparse
import logging
import multiprocessing
from typing import List

from mmpdblib import commandline as mmp

logging.basicConfig(level=logging.DEBUG)


def run_mmp(args: List[str]) -> None:
    logging.info('mmpdb arguments: {}'.format(' '.join(args)))

    mmp.main(args)


def build_fragments(smiles_file: str, fragments_file:str) -> None:
    assert smiles_file.endswith('.smi')
    assert fragments_file.endswith('.gz')

    n_proc = multiprocessing.cpu_count()
    args = ['fragment', '--in', 'smi', '--out', 'fragments.gz', '--num-jobs', str(n_proc), '--output',
            fragments_file, '--delimiter',
            'space', smiles_file]
    run_mmp(args)


def index_fragments(fragments_file: str, database_file:str) -> None:
    assert fragments_file.endswith('.gz')
    assert database_file.endswith('.mmpdb')

    args = ['index', fragments_file, '-o', database_file]
    run_mmp(args)


def add_properties(properties_file: str, database_file: str) -> None:
    assert database_file.endswith('.mmpdb')

    args = ['loadprops', '-p', properties_file, database_file]
    run_mmp(args)


def main():
    parser = argparse.ArgumentParser(description='Create MMPDB database from smiles and property files')
    parser.add_argument('--smiles_file', type=str, default=None, help='Smiles file', required=True)
    parser.add_argument('--properties_file', type=str, default=None, help='Properties for smiles file', required=True)
    parser.add_argument('-fragments_file', type=str, default=None,
                        help='Optional fragment output file- defaults to basename from smiles file')
    parser.add_argument('-database_file', type=str, default=None,
                        help='Optional database output file- defaults to basename from smiles file')
    args = parser.parse_args()

    smiles_file = args.smiles_file
    fragments_file = args.fragments_file
    if not fragments_file:
        fragments_file = '{}.fragments.gz'.format(smiles_file[:-4])
    database_file = args.database_file
    if not database_file:
        database_file = '{}.mmpdb'.format(smiles_file[:-4])

    build_fragments(smiles_file, fragments_file)
    index_fragments(fragments_file, database_file)
    add_properties(args.properties_file, database_file)

if __name__ == '__main__':
    main()