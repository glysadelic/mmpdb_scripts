import argparse
import csv
from typing import List, Optional
import common
import os.path


def process_csv(file: str, id_column: str, smiles_column: str, field_names: List[str],
                smiles_file: Optional[str], activity_file: Optional[str], encoding: str, sep: str) -> None:
    assert file.endswith('.csv')

    if not smiles_file:
        smiles_file = os.path.basename('{}.smi'.format(file[:-4]))
    if not activity_file:
        activity_file = os.path.basename('{}.props'.format(file[:-4]))

    with open(file, 'r', encoding=encoding) as fh, open(smiles_file, 'w') as sf, open(activity_file, 'w') as af:
        csv_reader = csv.reader(fh, delimiter=sep)

        fields = next(csv_reader)
        field_numbers = [i for i, f in enumerate(fields) if f in field_names]
        if len(field_names) != len(field_numbers):
            raise ValueError('CSV is missing property field defined in command line')

        id_field_no = next(
            (i for i, f in enumerate(fields) if f == id_column), -1)
        if id_field_no == -1:
            raise ValueError('No column for id field {}'.format(id_column))

        smiles_field_no = next(
            (i for i, f in enumerate(fields) if f == smiles_column), -1)
        if smiles_field_no == -1:
            raise ValueError('No column for id field {}'.format(smiles_column))

        af.write('ID')
        for f in field_numbers:
            af.write(' {}'.format(fields[f].replace(' ', '_')))
        af.write('\n')

        ids = set()
        smi_set = set()

        for row_num, row in enumerate(csv_reader):

            properties = [row[f] for f in field_numbers]
            properties = [f if f else '*' for f in properties]

            id = row[id_field_no]
            if not id:
                raise ValueError('No id in row {}'.format(row_num))
            if id in ids:
                raise ValueError('Duplicate entries for id {}'.format(id))
            ids.add(id)
            smiles = row[smiles_field_no]
            if not smiles:
                raise ValueError('No smiles in row {}'.format(row_num))
            if smiles in smi_set:
                print('Duplicate entries for smiles {}'.format(smiles))
            smi_set.add(smiles)
            smiles = common.fix_smiles(smiles)

            af.write('{} '.format(id))
            for property in properties:
                af.write('{} '.format(property))
            af.write('\n')
            sf.write('{} {}\n'.format(smiles, id))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Create input MMPDB files from CSV file')
    parser.add_argument('--file', type=str, help='CSV input file', required=True)
    parser.add_argument('--id_column', type=str,
                        help='ID column Name', required=True)
    parser.add_argument('--smiles_column', type=str,
                        help='smiles column Name', required=True)
    parser.add_argument('--field_names', nargs='*', type=str, default=None, required=True,
                        help='List of fields in the csv file that will be used as properties')
    parser.add_argument('--smiles_file', type=str, default=None, required=False,
                        help='Optional smiles output file- defaults to basename from csv file')
    parser.add_argument('--activity_file', type=str, default=None, required=False,
                        help='Optional properties output file- defaults to basename from csv file')
    parser.add_argument('--encoding', type=str, default='utf-8-sig', required=False,
                        help='Optional encoding for csv file default is utf-8-sig')
    parser.add_argument('--separator', type=str,
                        help='Optional separator character for CSV input default is ","', default=',', required=False)
    args = parser.parse_args()

    process_csv(args.file, args.id_column, args.smiles_column, args.field_names,
                args.smiles_file, args.activity_file, args.encoding, args.separator)
