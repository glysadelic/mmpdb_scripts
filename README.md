# Helper scripts for constructing MMPDB databases

Scripts to facilitate construction of MMPDB databases from CSV files or SDF files.

Building the MMPDB database is a 2 stage process:

1. Create MMP inputs (smiles and property files) from either a CSV or SDF file
2. Build the MMPDB database using a wrapper script which calls MMPDB functions to fragment the structures, index the fragments and add the properties.

## Building a Python environment

The following Linux commands require an Anaconda python 3 installation and create a Conda environment with RDKit and MMPDB packages and dependencies

```shell
conda create -y -c conda-forge -n mmpdb-util rdkit  # Create environment with RDKit packages
conda activate mmpdb-util                           # Activate new environment
conda install -y scipy ujson                        # Install MMPDB conda dependencies
pip install apsw                                    # Install MMPDB pip dependency
git clone https://github.com/rdkit/mmpdb.git        # Clone MMPDB source from github
cd mmpdb           
python setup.py install                             # Install MMPDB into environment
cd ..
rm -rf mmpdb                                        # remove MMPDB source
```

## Building an MMPDB database from a CSV file

### Creating MMPDB input files from a CSV file

The CSV file should have a column of smiles, a column of IDs  and one or more property columns.

The script `csv_to_mmp_input.py` will convert a CSV file to a smiles and property files.

```console
$ python csv_to_mmp_input.py -h
usage: csv_to_mmp_input.py [-h] --file FILE --id_column ID_COLUMN --smiles_column SMILES_COLUMN --field_names [FIELD_NAMES ...]
                           [--smiles_file SMILES_FILE] [--activity_file ACTIVITY_FILE] [--encoding ENCODING] [--separator SEPARATOR]

Create input MMPDB files from CSV file

optional arguments:
  -h, --help            show this help message and exit
  --file FILE           CSV input file
  --id_column ID_COLUMN
                        ID column Name
  --smiles_column SMILES_COLUMN
                        smiles column Name
  --field_names [FIELD_NAMES ...]
                        List of fields in the csv file that will be used as properties
  --smiles_file SMILES_FILE
                        Optional smiles output file- defaults to basename from csv file
  --activity_file ACTIVITY_FILE
                        Optional properties output file- defaults to basename from csv file
  --encoding ENCODING   Optional encoding for csv file default is utf-8-sig
  --separator SEPARATOR
                        Optional separator character for CSV input default is ","
```

The arguments `--file`, `--id_column`, `--smiles_column` and `--field_names` are required.

For example, to create smiles and property files from the Gyrase dataset:

```shell
python csv_to_mmp_input.py \
    --file data/AZ_Pyrrolamides_4OE_noqualifiers.csv \
    --id_column External_ID \
    --smiles_column 'Molecule SMILES' \
    --field_names 'SAU Gyr IC50' 'Sau516 MIC' \
                   'Mean Human Prot binding (% free)' 'Mean LogD'
```

This creates two text files `AZ_Pyrrolamides_4OE_noqualifiers.smi` and `AZ_Pyrrolamides_4OE_noqualifiers.props`.

### Building the MMPDB database from property and smiles files

The script `build_mmp_database.py` creates an MMPDB Sqlite3 database from the smiles and property files created in the previous step.

```console
$ python build_mmp_database.py -h
usage: build_mmp_database.py [-h] --smiles_file SMILES_FILE --properties_file PROPERTIES_FILE [-fragments_file FRAGMENTS_FILE]
                             [-database_file DATABASE_FILE]

Create MMPDB database from smiles and property files

optional arguments:
  -h, --help            show this help message and exit
  --smiles_file SMILES_FILE
                        Smiles file
  --properties_file PROPERTIES_FILE
                        Properties for smiles file
  -fragments_file FRAGMENTS_FILE
                        Optional fragment output file- defaults to basename from smiles file
  -database_file DATABASE_FILE
                        Optional database output file- defaults to basename from smiles file                      
```

Given the two text files from the previous section the MMPDB database for the Gyrase dataset is created using this command.

```console
$ python build_mmp_database.py --smiles_file AZ_Pyrrolamides_4OE_noqualifiers.smi --properties_file AZ_Pyrrolamides_4OE_noqualifiers.props
INFO:root:mmpdb arguments: fragment --in smi --out fragments.gz --num-jobs 12 --output AZ_Pyrrolamides_4OE_noqualifiers.fragments.gz --delimiter space AZ_Pyrrolamides_4OE_noqualifiers.smi
Fragmented record 202/981 (20.6%)[18:25:37] Conflicting single bond directions around double bond at index 19.
INFO:root:mmpdb arguments: index AZ_Pyrrolamides_4OE_noqualifiers.fragments.gz -o AZ_Pyrrolamides_4OE_noqualifiers.mmpdb
INFO:root:mmpdb arguments: loadprops -p AZ_Pyrrolamides_4OE_noqualifiers.props AZ_Pyrrolamides_4OE_noqualifiers.mmpdb
Using dataset: MMPs from 'AZ_Pyrrolamides_4OE_noqualifiers.fragments.gz'
Reading properties from 'AZ_Pyrrolamides_4OE_noqualifiers.props'
Read 4 properties for 981 compounds from 'AZ_Pyrrolamides_4OE_noqualifiers.props'
17 compounds from 'AZ_Pyrrolamides_4OE_noqualifiers.props' are not in the dataset at 'AZ_Pyrrolamides_4OE_noqualifiers.mmpdb'
Imported 964 'SAU_Gyr_IC50' records (964 new, 0 updated).
Imported 766 'Sau516_MIC' records (766 new, 0 updated).
Imported 529 'Mean_Human_Prot_binding_(%_free)' records (529 new, 0 updated).
Imported 518 'Mean_LogD' records (518 new, 0 updated).
Generated 307821 rule statistics (108071 rule environments, 4 properties)
Number of rule statistics added: 307821 updated: 0 deleted: 0
Loaded all properties and re-computed all rule statistics.
```

Once the script finished the database can be found in `AZ_Pyrrolamides_4OE_noqualifiers.mmpdb`. There is also a file called `AZ_Pyrrolamides_4OE_noqualifiers.fragments.gz` which can safely be deleted once the database is crested.

## Building an MMPDB database from an SDF file

### Creating MMPDB input files from an SDF file

The script `sdf_to_mmp_input.py` will convert an SDF file to smiles and property files.

```console
$ python sdf_to_mmp_input.py --help
usage: sdf_to_mmp_input.py [-h] --file FILE --id_field ID_FIELD --field_names [FIELD_NAMES ...] [--smiles_file SMILES_FILE] [--activity_file ACTIVITY_FILE]

Create input MMPDB files from CSV file

optional arguments:
  -h, --help            show this help message and exit
  --file FILE           CSV input file
  --id_field ID_FIELD   SD field for structure id
  --field_names [FIELD_NAMES ...]
                        List of fields in the sdf file that will be used as properties
  --smiles_file SMILES_FILE
                        Optional smiles output file- defaults to basename from sdf file
  --activity_file ACTIVITY_FILE
                        Optional properties output file- defaults to basename from sdf file
```

The arguments `--file`, `--id_field` and `--field_names` are required.

For example, to create smiles and property files from the Chagas dataset:

```shell
python sdf_to_mmp_input.py --file data/chagas.sdf  --id_field 'Molecule Name' --field_names PIC50
```

The command creates two MMPDB input files: `chagas.smi` and `chagas.props`.

### Building the MMPDB database from property and smiles files (2)

Building the MMPDB database for the Chagas dataset (`chagas.mmpdb`) proceeds in the same manner as for the Gyrase dataset.

```console
$ python build_mmp_database.py --smiles_file chagas.smi --properties_file chagas.props
INFO:root:mmpdb arguments: fragment --in smi --out fragments.gz --num-jobs 12 --output chagas.fragments.gz --delimiter space chagas.smi
INFO:root:mmpdb arguments: index chagas.fragments.gz -o chagas.mmpdb
INFO:root:mmpdb arguments: loadprops -p chagas.props chagas.mmpdb
Using dataset: MMPs from 'chagas.fragments.gz'
Reading properties from 'chagas.props'
Read 1 properties for 741 compounds from 'chagas.props'
20 compounds from 'chagas.props' are not in the dataset at 'chagas.mmpdb'
Imported 721 'PIC50' records (721 new, 0 updated).
Generated 83460 rule statistics (83460 rule environments, 1 properties)
Number of rule statistics added: 83460 updated: 0 deleted: 0
Loaded all properties and re-computed all rule statistics.
```

## Datasets

The CSV file is a public Gryase dataset from Astra-Zeneca

The SDF file is a Chagas dataset sample made available by [CDD](https://github.com/cdd/bayseg). I added the PIC50 field by removing qualifiers from the uM Ic50 field then performing a negative log10 transform on molar concentration.
